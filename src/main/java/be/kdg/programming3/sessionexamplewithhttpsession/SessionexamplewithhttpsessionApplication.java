package be.kdg.programming3.sessionexamplewithhttpsession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SessionexamplewithhttpsessionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SessionexamplewithhttpsessionApplication.class, args);
    }

}
