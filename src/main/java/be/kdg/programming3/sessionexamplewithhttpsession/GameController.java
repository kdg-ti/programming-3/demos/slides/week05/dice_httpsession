package be.kdg.programming3.sessionexamplewithhttpsession;

import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/game")
public class GameController {
    @GetMapping("")
    public String getStart(){
        return "start";
    }

    @GetMapping("/throwdice")
    public String getThrowDice(HttpSession httpSession, String name){
        if (name!=null) {
            Player player = new Player();
            player.setName(name);
            httpSession.setAttribute("player", player);
        }
        Player player = (Player) httpSession.getAttribute("player");
        player.doThrow();
        return "throwdice";
    }
}
