package be.kdg.programming3.sessionexamplewithhttpsession;

import java.util.Random;

public class Player {
    private Random random = new Random();
    private String name;
    private int totalScore = 0;
    private int latestThrow1 = 0;
    private int latestThrow2 = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public int getLatestThrow1() {
        return latestThrow1;
    }

    public int getLatestThrow2() {
        return latestThrow2;
    }

    public void doThrow() {
        latestThrow1 = random.nextInt(1, 7);
        latestThrow2 = random.nextInt(1, 7);
        totalScore += latestThrow1 + latestThrow2;
    }
}
